//
// Test bench for the PC.
//

`timescale 1ns / 100ps 
 
module pc_tb;

reg clk;
reg [1:0] src_select;
reg [31:0] addr_in;
reg [12:0] relative_addr;
wire [31:0] pc;
wire [31:0] next_pc;

pc test_pc (
    .clk(clk),
    .src_select(src_select),
    .addr_in(addr_in),
    .relative_addr(relative_addr),
    .pc(pc),
    .next_pc(next_pc)
);

initial begin
    // Store in r1.
    clk = 0;
    src_select = 1;
    addr_in = 32'h12345678;
    relative_addr = 13'h123;

#20;
    clk = 1;

#20;
    clk = 0;
    src_select = 0;

#20;
    clk = 1;

#20;
    clk = 0;

#20;
    clk = 1;

#20;
    clk = 0;
    src_select = 2;

#20;
    clk = 1;

#20;
    clk = 0;

#20;
end

initial begin
    $dumpfile("obj/pc_tb.lxt");
    $dumpvars(0, pc_tb);
end

endmodule
