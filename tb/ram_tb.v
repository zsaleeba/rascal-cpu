//
// Test bench for RAM memory.
//

`timescale 1ns / 100ps 
 
module ram_tb;

reg clk, wen; 
reg [2:0] width;
reg [9:0] addr;
reg [31:0] wdata;
wire [31:0] rdata;

ram mem (
    .clk(clk),
    .wen(wen),
    .width(width),
    .addr(addr),
    .wdata(wdata),
    .rdata(rdata)
);

initial begin
    clk = 0;
    wen = 1;
    width = 2; // Word.
    addr = 0;

    // Store a word 12345678 at location 0.
    wdata = 32'h12345678;

#20;
    clk = 1;

    // Read it back.
#20;
    clk = 0;
    wen = 0;

#20;
    clk = 1;
    
    // Store a word deadbeef at location 0.
#20;
    wdata = 32'hdeadbeef;
    clk = 0;
    wen = 1;

#20;
    clk = 1;
    
    // Read a word.
#20;
    wen = 0;
    width = 2; // Word.
    wdata = 32'hcafebabe;
    clk = 0;

#20;
    clk = 1;

    // Read a signed byte.
#20;
    width = 0;
    clk = 0;
    wen = 0;

#20;
    clk = 1;

    // Read a signed halfword.
#20;
    width = 1;
    clk = 0;
    wen = 0;

#20;
    clk = 1;


    // Read an unsigned byte.
#20;
    width = 4;
    clk = 0;
    wen = 0;

#20;
    clk = 1;

    // Read an unsigned halfword.
#20;
    width = 5;
    clk = 0;
    wen = 0;

#20;
    clk = 1;

    // Write a byte.
#20;
    clk = 0;
    width = 0;
    wen = 1;
    wdata = 32'h12233445;

#20;
    clk = 1;

    // Read it back as a word.
#20;
    width = 2;
    clk = 0;
    wen = 0;

#20;
    clk = 1;

    // Write a halfword.
#20;
    clk = 0;
    width = 1;
    wen = 1;
    wdata = 32'h98765432;

#20;
    clk = 1;

    // Read it back as a word.
#20;
    width = 2;
    clk = 0;
    wen = 0;

#20;
    clk = 1;


#20;
    clk = 0;


#20;
    clk = 1;


#20;
end

initial begin
    $dumpfile("obj/ram_tb.lxt");
    $dumpvars(0, ram_tb);
end

endmodule
