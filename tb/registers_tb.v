//
// Test bench for the ALU.
//

`timescale 1ns / 100ps 
 
module registers_tb;

reg clk, wen;
reg [4:0] waddr, raddr1, raddr2;
reg [31:0] wdata;
wire [31:0] rdata1, rdata2;

registers test_reg (
    .clk(clk),
    .wen(wen),
    .waddr(waddr),
    .raddr1(raddr1),
    .raddr2(raddr2),
    .wdata(wdata),
    .rdata1(rdata1),
    .rdata2(rdata2)
);

initial begin
    // Store in r1.
    clk = 0;
    wen = 1;
    waddr = 1;
    wdata = 32'h12345678;
    raddr1 = 1;
    raddr2 = 2;

#20;
    clk = 1;

#20;
    // Read back.
    wen = 0;
    clk = 0;

#20;
    clk = 1;

#20;
    // Store in r2.
    clk = 0;
    wen = 1;
    waddr = 2;
    wdata = 32'hdeadbeef;

#20;
    clk = 1;

#20;
    // Read back.
    wen = 0;
    clk = 0;

#20;
    clk = 1;

#20;
    // Store in r0.
    clk = 0;
    wen = 1;
    waddr = 0;
    wdata = 32'hcafebabe;

#20;
    clk = 1;

#20;
    // Read back.
    wen = 0;
    clk = 0;
    raddr1 = 0;

#20;
    clk = 1;

#20;
end

initial begin
    $dumpfile("obj/registers_tb.lxt");
    $dumpvars(0, registers_tb);
end

endmodule
