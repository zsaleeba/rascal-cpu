//
// Test bench for the ALU.
//

`timescale 1ns / 100ps 
 
module alu_tb;

reg [0:2] op;
reg alt_op;
reg [31:0] src1, src2;
wire [31:0] dest;
wire eq, lt, ltu;

alu test_alu (
    .op(op),
    .alt_op(alt_op),
    .src1(src1),
    .src2(src2),
    .dest(dest),
    .eq(eq),
    .lt(lt),
    .ltu(ltu)
);

initial begin
    // Add.
    op = 0;
    alt_op = 0;
    src1 = 32'h42536475;
    src2 = 32'h12345678;

#20;
    // Subtract.
    alt_op = 1;


#20;
    // XOR.
    op = 4;
    alt_op = 0;


#20;
    // OR.
    op = 6;


#20;
    // AND.
    op = 7;


#20;
    // SLT.
    op = 2;

#20;
    src1 = 32'hdeadca22;
    src2 = 32'h12345678;

#20;
    // SLTU.
    op = 3;


#20;
    // SLL.
    op = 1;
    src1 = 32'h87654321;
    src2 = 8;

#20;
    // SRL.
    op = 5;
    alt_op = 0;

#20;
    // SRA.
    op = 5;
    alt_op = 1;

#20;
    // eq output.
    op = 0;
    alt_op = 0;
    src1 = 32'h98765432;
    src2 = 32'h98765432;

#20;
    // lt output.
    src1 = 32'h54433221;
    src2 = 32'h65544332;

#20;
    // ltu output.
    src1 = 32'hbccddefa;
    src2 = 32'h65544332;

#20;
end

initial begin
    $dumpfile("obj/alu_tb.lxt");
    $dumpvars(0, alu_tb);
end

endmodule
