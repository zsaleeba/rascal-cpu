// Sets uart_out to 0x48.
00000000: 0080006f   reset_vec:    jal  start
00000004: 00000000   ecall_vec:    nop
00000008: 00000000   ebreak_vec:   nop
0480000c: 04800093   start:        addi x1, x0, #0x48
00000010: 20000137                 lui  x2, #0x20000
00000014: 00110023                 sb   x1, x2, #0

// Writes "Hello world!\n to uart_out.
00000000: 0080006f   reset_vec:    jal  start
00000004: 00000000   ecall_vec:    nop
00000008: 00000000   ebreak_vec:   nop
0000000c: 00d00093   start:        addi x1, x0, #0x0d       // x1 = number of bytes.
00000010: 03000113                 addi x2, x0, #0x28       // x2 = address of string.
00000014: 200001b7                 lui  x3, #0x20000        // x3 = address of uart.
00000018: 00010203   loop:         lb   x4, x2, #0          // Get a byte from the string.
0000001c: 00418023                 sb   x3, x4, #0          // Send it to the UART.
00000020: 00110113                 addi x2, x2, #1          // Increment the string pointer.
00000024: fff08093                 subi x1, x1, #1          // Decrement the character counter.
00000028: fe101be3                 bne  x1, x0, loop        // Loop until we're finished. (-0x14)
0000002c: 00c00067                 jalr x0, x0, start       // Go back to the start. (-0x24)

00000030: 6c6c6548   hello:        db   "Hello world!\n"
00000034: 6f77206f
00000038: 21646c72
0000003c: 0000000a
