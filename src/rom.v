//
// Rascal CPU program ROM memory.
//

module rom (
    input [11:0] iaddr,
    output [31:0] idata,
    input [2:0] width,
    input [11:0] raddr,
    output reg [31:0] rdata
);
    reg [31:0] instr_mem [0:1023];    // Four bytes * 1024.
    wire [1:0] bit_width;
    wire [9:0] iaddr_w;
    wire [9:0] raddr_w;
    wire raddr_h;
    wire [1:0] raddr_b;
    reg  [7:0] rbyte;
    reg  [15:0] rhalf;

    // Argument breakdown.
    assign bit_width = width[1:0];
    assign iaddr_w = iaddr[11:2];
    assign raddr_w = raddr[11:2];
    assign raddr_h = raddr[1];
    assign raddr_b = raddr[1:0];

    // Read instructions.
    assign idata = instr_mem[iaddr_w];

    // Read program data.
    always @* begin
        // Read bytes, halfwords and words.
        case (raddr_b)
            2'h0:    rbyte <= instr_mem[raddr_w][7:0];
            2'h1:    rbyte <= instr_mem[raddr_w][15:8];
            2'h2:    rbyte <= instr_mem[raddr_w][23:16];
            2'h3:    rbyte <= instr_mem[raddr_w][31:24];
        endcase

        case (raddr_h)
            2'h0:    rhalf <= instr_mem[raddr_w][15:0];
            2'h1:    rhalf <= instr_mem[raddr_w][31:16];
        endcase

        case (width)
            3'h0:    rdata <= $signed(rbyte);       // Signed byte.
            3'h1:    rdata <= $signed(rhalf);       // Signed halfword.
            3'h4:    rdata <= rbyte;                // Unsigned byte.
            3'h5:    rdata <= rhalf;                // Unsigned halfword.
            default: rdata <= instr_mem[raddr_w];   // Word.
        endcase
    end

    initial begin
        $readmemh("tb/rascal-rom.mem", instr_mem);
    end

endmodule
