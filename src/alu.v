//
// ALU for Rascal CPU. 
//
// This functionality is based on the RISC-V spec.
//
// +--------+--------+--------+--------------------------------+
// |   op   | alt_op | opcode | description                    |
// +--------+--------+--------+--------------------------------+
// |   000  |    0   |  ADD   | dest = src1 + src2             |
// |   000  |    1   |  SUB   | dest = src1 - src2             |
// |   001  |    -   |  SLL   | dest = src1 << src2            |
// |   010  |    -   |  SLT   | dest = src1 < src2 (signed)    |
// |   011  |    -   |  SLTU  | dest = src1 < src2 (unsigned)  |
// |   100  |    -   |  XOR   | dest = src1 ^ src2             |
// |   101  |    0   |  SRL   | dest = src1 >> src2 (unsigned) |
// |   101  |    1   |  SRA   | dest = src1 >> src2 (signed)   |
// |   110  |    -   |  OR    | dest = src1 | src2             |
// |   111  |    -   |  AND   | dest = src1 & src2             |
// +--------+--------+--------+--------------------------------+
//

module alu (
    input [0:2] op,
    input alt_op,
    input [31:0] src1, src2,
    output reg [31:0] dest,
    output eq, lt, ltu
);
    always @* begin
        case (op)
            3'h0: begin
                // Add or subtract.
                if (alt_op == 0) begin
                    // ADD.
                    dest = src1 + src2;
                end
                else begin
                    // SUB.
                    dest = src1 - src2;
                end
            end

            3'h1: begin
                // SLL.
                dest = src1 << src2;
            end

            3'h2: begin
                // SLT.
                dest = {31'b0, $signed(src1) < $signed(src2)};
            end

            3'h3: begin
                // SLTU.
                dest = {31'b0, $unsigned(src1) < $unsigned(src2)};
            end

            3'h4: begin
                // XOR.
                dest = src1 ^ src2;
            end

            3'h5: begin
                // SRL/SRA.
                if (alt_op == 0) begin
                    // SRL.
                    dest = src1 >> src2;
                end
                else begin
                    // SRA.
                    dest = $signed(src1) >>> src2;
                end
            end

            3'h6: begin
                // OR.
                dest = src1 | src2;
            end

            3'h7: begin
                // AND.
                dest = src1 & src2;
            end
        endcase
    end

    // Comparison operators for branching.
    assign eq  = src1 == src2;
    assign lt  = $signed(src1) < $signed(src2);
    assign ltu = $unsigned(src1) < $unsigned(src2);

endmodule
