//
// Program counter and instruction address calculation for Rascal CPU.
//
// The PC really only has 30 usable bits since it only allows longword 
// (4 byte) instruction addressing but RISC-V has 32 bit byte addressing.
//
// Relative addresses are based on RISC-V word (2 byte) addressing with the
// lowest bit being ignored.
//

module pc (
        input clk, 
        input reset,
        input [1:0] src_select,
        input [31:0] addr_in,
        input [12:0] relative_addr,
        output reg [31:0] pc,
        output [31:0] next_pc
);

    wire [31:0] branch_offset;
    
    assign branch_offset = $signed({relative_addr[12:1], 2'b00});
    assign next_pc = {pc[31:2], 2'b0} + 4;

    // Reset handling.
    always @(posedge reset) 
    begin
        pc <= 0;
    end

    // PC changes on clk edge.
    always @(posedge clk) 
    begin
		case (src_select)
			3'h0: begin
				pc <= next_pc;
			end
            
            3'h1: begin
                pc <= {addr_in[31:2], 2'b0};
            end
            
            3'h2: begin
                pc <= next_pc + branch_offset;
            end
        endcase
    end
endmodule
