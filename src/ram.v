//
// RAM memory for Rascal CPU.
//

module ram (
    input clk, wen, 
    input [2:0] width,
    input [11:0] addr,
    input [31:0] wdata,
    output reg [31:0] rdata
);
    reg [7:0] mem [0:3] [0:1023];    // Four bytes * 1024.
    wire [1:0] bit_width;
    wire [9:0] addr_w;
    wire addr_h;
    wire [1:0] addr_b;

    // Argument breakdown.
    assign bit_width = width[1:0];
    assign no_sign_extend = width[2];
    assign addr_w = addr[11:2];
    assign addr_h = addr[1];
    assign addr_b = addr[1:0];

    always @(posedge clk) begin
        if (wen)
        begin
            // Write bytes, halfwords and words.
            case (bit_width)
                2'h0: begin
                    // Byte.
                    mem[addr_b][addr_w] <= wdata[7:0];
                end

                2'h1: begin
                    // Halfword.
                    mem[{addr_h, 1'b0}][addr_w] <= wdata[7:0];
                    mem[{addr_h, 1'b1}][addr_w] <= wdata[15:8];
                end

                default: begin
                    // Word.
                    mem[2'h0][addr_w] <= wdata[7:0];
                    mem[2'h1][addr_w] <= wdata[15:8];
                    mem[2'h2][addr_w] <= wdata[23:16];
                    mem[2'h3][addr_w] <= wdata[31:24];
                end
            endcase
        end
        
        // Read bytes, halfwords and words.
        case (width)
            3'h0:    rdata <= $signed(mem[addr_b][addr_w]);                                                           // Signed byte.
            3'h1:    rdata <= $signed({mem[{addr_h, 1'b1}][addr_w], mem[{addr_h, 1'b0}][addr_w]});                    // Signed halfword.
            3'h4:    rdata <= {24'b0, mem[addr_b][addr_w]};                                                           // Unsigned byte.
            3'h5:    rdata <= {16'b0, mem[{addr_h, 1'b1}][addr_w], mem[{addr_h, 1'b0}][addr_w]};                      // Unsigned halfword.
            default: rdata <= {mem[{2'h3}][addr_w], mem[{2'h2}][addr_w], mem[{2'h1}][addr_w], mem[{2'h0}][addr_w]};   // Word.
        endcase

    end
endmodule
