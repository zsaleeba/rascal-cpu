//
// Instruction decoding for Rascal CPU.
//
// This is based on the RISC-V uncompressed instruction format.
//

module instr (
    input      [31:0] iword,
    input             alu_eq,
    input             alu_lt,
    input             alu_ltu,
    output reg  [1:0] pc_src_select,
    output      [2:0] mem_width,
    output reg        mem_wen,
    output reg [31:0] imm,
    output     [12:0] relative_addr,
    output reg  [4:0] reg_waddr,
    output reg  [4:0] reg_raddr1,
    output reg  [4:0] reg_raddr2,
    output reg  [1:0] reg_wdata_sel,
    output reg        reg_wen,
    output reg  [2:0] alu_op,
    output reg        alu_alt_op,
    output reg        alu_src1_sel,
    output reg        alu_src2_sel
);

    wire [6:0]  opcode;
    wire [4:0]  rd;
    wire [2:0]  funct3;
    wire [4:0]  rs1;
    wire [4:0]  rs2;
    wire [6:0]  funct7;
    wire [11:0] i_imm12;        // I instruction type, 12 bit immediate value.
    wire [11:0] s_imm12;        // S instruction type, 12 bit immediate value.
    wire [19:0] u_imm20;        // U instruction type, 20 bit immediate value.
    wire [12:0] b_imm13;        // B instruction type, 13 bit relative address.
    wire [21:0] j_imm21;        // J instruction type, 21 bit relative address.

    // Instruction word break-down.
    assign opcode     = iword[6:0];
    assign rd         = iword[11:7];
    assign funct3     = iword[14:12];
    assign rs1        = iword[19:15];
    assign rs2        = iword[24:20];
    assign funct7     = iword[31:25];
    assign i_imm12    = iword[31:20];
    assign s_imm12    = {iword[31:25], iword[11:7]};
    assign u_imm20    = iword[31:12];
    assign b_imm13    = {iword[31], iword[7], iword[30:25], iword[11:8], 1'b0};
    assign j_imm21    = {iword[31], iword[19:12], iword[20], iword[30:21], 1'b0};
    
    assign mem_width  = funct3;
    assign relative_addr = b_imm13;

    // Handle each instruction type.
    always @*
    begin
        case (opcode)
            7'h03: begin
                // LB / LH / LW / LBU / LHU.
                pc_src_select <= 0;      // PC <= PC + 4
                mem_wen <= 0;
                imm <= $signed(i_imm12); // imm = i_imm12
                reg_raddr1 <= rs1;       // reg.raddr1 = rs1
                reg_raddr2 <= 0; 
                reg_waddr <= rd;         // reg.waddr = rd
                reg_wdata_sel <= 2'h2;   // reg.wdata = data_ram.rdata
                reg_wen <= 1;            // reg.wen = 1
                alu_src1_sel <= 0;       // alu_src1 = reg_rdata1
                alu_src2_sel <= 1;       // alu_src2 = imm
                alu_op <= 3'h0;          // alu_op = ADD / SUB
                alu_alt_op <= 0;         // alu_alt_op = ADD
            end

            7'h23: begin
                // SB / SH / SW.
                pc_src_select <= 0;      // PC <= PC + 4
                mem_wen <= 1;            // mem_wen = 1
                imm <= $signed(s_imm12); // imm = s_imm12
                reg_raddr1 <= rs1;       // reg.raddr1 = rs1
                reg_raddr2 <= rs2;       // reg.raddr2 = rs2
                reg_waddr <= 0; 
                reg_wdata_sel <= 2'h0; 
                reg_wen <= 0; 
                alu_src1_sel <= 0;       // alu_src1 = reg_rdata1
                alu_src2_sel <= 1;       // alu_src2 = imm
                alu_op <= 3'h0;          // alu_op = ADD / SUB
                alu_alt_op <= 0;         // alu_alt_op = ADD
            end

            7'h37: begin
                // LUI.
                pc_src_select <= 0;      // PC <= PC + 4
                mem_wen <= 0; 
                imm <= u_imm20 << 12;    // imm = u_imm20 << 12
                reg_raddr1 <= 0;         // reg.raddr1 = 0
                reg_raddr2 <= 0; 
                reg_waddr <= rd;         // reg.waddr = rd
                reg_wdata_sel <= 2'h0;   // reg.wdata = alu_out
                reg_wen <= 1;            // reg.wen = 1
                alu_src1_sel <= 0;       // alu_src1 = reg_rdata1 (0)
                alu_src2_sel <= 1;       // alu_src2 = imm
                alu_op <= 3'h6;          // alu_op = OR
                alu_alt_op <= 0;
            end

            7'h13: begin
                // ADDI / SLLI / SRLI / SLTI / XORI / ORI / ANDI.
                pc_src_select <= 0;      // PC <= PC + 4
                mem_wen <= 0; 
                imm <= $signed(i_imm12); // imm = i_imm12
                reg_raddr1 <= rs1;       // reg.raddr1 = rs1
                reg_raddr2 <= 0; 
                reg_waddr <= rd;         // reg.waddr = rd
                reg_wdata_sel <= 2'h0;   // reg.wdata = alu_out
                reg_wen <= 1;            // reg.wen = 1
                alu_src1_sel <= 0;       // alu_src1 = reg_rdata1
                alu_src2_sel <= 1;       // alu_src2 = imm
                alu_op <= funct3;        // alu_op = funct3
                alu_alt_op <= 0;         // alu_alt_op = 0
            end

            7'h33: begin
                // ADD / SUB / SLL / SLT / SLTU / XOR / SRL / SRA / OR / AND.
                pc_src_select <= 0;      // PC <= PC + 4
                mem_wen <= 0; 
                imm <= 0; 
                reg_raddr1 <= rs1;       // reg.raddr1 = rs1
                reg_raddr2 <= rs2;       // reg.raddr2 = rs2
                reg_waddr <= rd;         // reg.waddr = rd
                reg_wdata_sel <= 2'h0;   // reg.wdata = alu_out
                reg_wen <= 1;            // reg.wen = 1
                alu_src1_sel <= 0;       // alu_src1 = reg_rdata1
                alu_src2_sel <= 0;       // alu_src2 = reg_rdata2
                alu_op <= funct3;        // alu_op = funct3
                alu_alt_op <= iword[30]; // alu_alt_op = iword[30]
            end

            7'h63: begin
                // BEQ / BNE / BLT / BGE / BLTU / BGEU.
                case (funct3)
                    3'h0: pc_src_select <= (alu_eq ? 2 : 0);   // BEQ.
                    3'h1: pc_src_select <= (alu_eq ? 0 : 2);   // BNE.
                    3'h4: pc_src_select <= (alu_lt ? 2 : 0);   // BLT.
                    3'h5: pc_src_select <= (alu_lt ? 0 : 2);   // BGE.
                    3'h6: pc_src_select <= (alu_ltu ? 2 : 0);  // BLTU.
                    3'h7: pc_src_select <= (alu_ltu ? 0 : 2);  // BGEU.
                endcase

                mem_wen <= 0;
                imm <= 0;
                reg_raddr1 <= rs1;       // reg.raddr1 = rs1
                reg_raddr2 <= rs2;       // reg.raddr2 = rs2
                reg_waddr <= 0; 
                reg_wdata_sel <= 2'h0; 
                reg_wen <= 0; 
                alu_src1_sel <= 0;       // alu_src1 = reg_rdata1
                alu_src2_sel <= 0;       // alu_src2 = reg_rdata2
                alu_op <= 3'h0;          // alu_op = ADD / SUB
                alu_alt_op <= 1;         // alu_alt_op = SUB
            end

            7'h6f: begin
                // JAL.
                pc_src_select <= 2'h1;   // PC <= alu_out
                mem_wen <= 0; 
                imm <= $signed(j_imm21); // imm = j_imm21
                reg_raddr1 <= 0;
                reg_raddr2 <= 0;
                reg_waddr <= rd;         // reg.waddr = rd
                reg_wdata_sel <= 2'h1;   // reg.wdata = PC + 4
                reg_wen <= 1;            // reg.wen = 1
                alu_src1_sel <= 1;       // alu_src1 = PC + 4
                alu_src2_sel <= 1;       // alu_src2 = imm
                alu_op <= 3'h0;          // alu_op = ADD / SUB
                alu_alt_op <= 0;         // alu_alt_op = ADD
            end

            7'h67: begin
                // JALR.
                pc_src_select <= 2'h1;   // PC <= alu_out
                mem_wen <= 0; 
                imm <= $signed(i_imm12); // imm = i_imm12
                reg_raddr1 <= 0;
                reg_raddr2 <= 0;
                reg_waddr <= rd;         // reg.waddr = rd
                reg_wdata_sel <= 2'h1;   // reg.wdata = PC + 4
                reg_wen <= 1;            // reg.wen = 1
                alu_src1_sel <= 0;       // alu_src1 = reg_rdata1
                alu_src2_sel <= 1;       // alu_src2 = imm
                alu_op <= 3'h0;          // alu_op = ADD / SUB
                alu_alt_op <= 0;         // alu_alt_op = ADD
            end

            7'h17: begin
                // AUIPC.
                pc_src_select <= 0;      // PC <= PC + 4
                mem_wen <= 0; 
                imm <= u_imm20 << 12;    // imm = u_imm20 << 12
                reg_raddr1 <= 0;
                reg_raddr2 <= 0;
                reg_waddr <= rd;         // reg.waddr = rd
                reg_wdata_sel <= 2'h0;   // reg.wdata = alu_out
                reg_wen <= 1;            // reg.wen = 1
                alu_src1_sel <= 1;       // alu_src1 = PC + 4
                alu_src2_sel <= 1;       // alu_src2 = imm
                alu_op <= 3'h0;          // alu_op = ADD / SUB
                alu_alt_op <= 0;         // alu_alt_op = ADD
            end

            7'h73: begin
                if (funct3 == 3'b0) begin
                    // ECALL / EBREAK.
                    pc_src_select <= 2'h1;   // PC <= alu_out
                    mem_wen <= 0; 
                    imm <= (i_imm12 == 0) ? 4 : 8; // ECALL / EBREAK vectors
                    reg_raddr1 <= 0;         // reg.raddr1 = 0
                    reg_raddr2 <= 0;
                    reg_waddr <= 0;
                    reg_wdata_sel <= 2'h0;
                    reg_wen <= 0;
                    alu_src1_sel <= 0;       // alu_src1 = reg_rdata1 (0)
                    alu_src2_sel <= 1;       // alu_src2 = imm
                    alu_op <= 3'h6;          // alu_op = OR
                    alu_alt_op <= 0;
                end
                else begin
                    // CSRRW / CSRRS / CSRRC / CSRRWI / CSRRSI / CSRRCI.
                    pc_src_select <= 0;      // PC <= PC + 4
                    mem_wen <= 0;
                    imm <= 0;                // imm = 0
                    reg_raddr1 <= 0;         // reg.raddr1 = 0
                    reg_raddr2 <= 0;
                    reg_waddr <= rd;         // reg.waddr = rd
                    reg_wdata_sel <= 2'h0;   // reg.wdata = alu_out
                    reg_wen <= 1;            // reg.wen = 1
                    alu_src1_sel <= 0;       // alu_src1 = reg_rdata1 (0)
                    alu_src2_sel <= 1;       // alu_src2 = imm
                    alu_op <= 3'h6;          // alu_op = OR
                    alu_alt_op <= 0;
                end
            end

            default: begin
                // NOP.
                pc_src_select <= 0;     // PC <= PC + 4
                mem_wen <= 0;
                imm <= 0;
                reg_raddr1 <= 0;
                reg_raddr2 <= 0;
                reg_waddr <= 0;
                reg_wdata_sel <= 2'h0;
                reg_wen <= 0;
                alu_src1_sel <= 0;
                alu_src2_sel <= 0;
                alu_op <= 3'h0;
                alu_alt_op <= 0;
            end
        endcase
    end
endmodule
