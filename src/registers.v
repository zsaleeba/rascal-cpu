//
// Register set for Rascal CPU. Dual read, single write.
//

module registers (
        input clk, wen,
        input [4:0] waddr, raddr1, raddr2,
        input [31:0] wdata,
        output reg [31:0] rdata1, rdata2
);
        reg [31:0] mem [0:31];

        // Async reads.
        always @* 
        begin
                if (raddr1 == 0)
                        rdata1 <= 0;
                else
                        rdata1 <= mem[raddr1];

                if (raddr2 == 0)
                        rdata2 <= 0;
                else
                        rdata2 <= mem[raddr2];
        end

        // Sync writes.
        always @(posedge clk) 
        begin
                if (wen)
                        mem[waddr] <= wdata;
        end
endmodule
