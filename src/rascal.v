//
// Top level interconnection of the Rascal CPU.
//

module rascal (
    input clk, 
    input reset,
    output [31:0] iaddr, 
    output [31:0] idata,
    output reg [31:0] uart_out,
    output reg        uart_send
);
    wire [31:0] pc;
    wire [31:0] next_pc;
    wire [31:0] idata;
    wire [1:0]  pc_src_select;
    wire [31:0] mem_addr;
    wire [31:0] mem_wdata;
    wire [31:0] mem_rdata;
    wire [2:0]  mem_width;
    wire        mem_wen;
    wire [31:0] ram_rdata;
    wire [31:0] rom_idata;
    wire [31:0] rom_rdata;
    wire [31:0] imm;
    wire [12:0] relative_addr;
    wire [4:0]  reg_waddr;
    wire [4:0]  reg_raddr1;
    wire [4:0]  reg_raddr2;
    wire [1:0]  reg_wdata_sel;
    reg  [31:0] reg_wdata;
    wire [31:0] reg_rdata1;
    wire [31:0] reg_rdata2;
    wire        reg_wen;
    wire [2:0]  alu_op;
    wire        alu_alt_op;
    wire        alu_src1_sel;
    wire        alu_src2_sel;
    wire [31:0] alu_out;
    wire        alu_eq;
    wire        alu_lt;
    wire        alu_ltu;
    wire        uart_write;

    /* Program counter. */
    pc pc_module(
        .clk(clk), 
        .reset(reset),
        .src_select(pc_src_select),
        .addr_in(alu_out),
        .relative_addr(relative_addr),
        .pc(pc),
        .next_pc(next_pc)
    );

    /* Instruction ROM memory. Located at 0x00000xxx. */
    assign idata = (pc[31:12] == 20'h00000) ? rom_idata : 0;
    rom instr_rom(
        .iaddr(pc[11:0]),
        .idata(rom_idata),
        .width(mem_width),
        .raddr(mem_addr[11:0]),
        .rdata(rom_rdata)
    );

    /* Data RAM. Located at 0x10000xxx. */
    assign mem_wdata = reg_rdata2;
    assign mem_rdata = (mem_addr[31:12] == 20'h10000) ? ram_rdata : ((mem_addr[31:12] == 20'h00000) ? rom_rdata : 0);

    ram data_ram(
        .clk(clk), 
        .wen(mem_wen && (mem_addr[31:12] == 20'h10000)),
        .width(mem_width),
        .addr(mem_addr[11:0]),
        .wdata(mem_wdata),
        .rdata(ram_rdata)
    );

    /* Register set. */
    registers registers_module(
        .clk(clk), 
        .wen(reg_wen),
        .waddr(reg_waddr), 
        .raddr1(reg_raddr1), 
        .raddr2(reg_raddr2),
        .wdata(reg_wdata),
        .rdata1(reg_rdata1),
        .rdata2(reg_rdata2)
    );

    /* Input multiplexer to the register write data. */
    always @*
    begin
        case (reg_wdata_sel)
            2'h0:    reg_wdata = alu_out;
            2'h1:    reg_wdata = next_pc;
            2'h2:    reg_wdata = mem_rdata;
        endcase
    end

    /* ALU. */
    alu alu_module(
        .op(alu_op),
        .alt_op(alu_alt_op),
        .src1(alu_src1_sel ? next_pc : reg_rdata1), 
        .src2(alu_src2_sel ? imm : reg_rdata2),
        .dest(alu_out),
        .eq(alu_eq), 
        .lt(alu_lt), 
        .ltu(alu_ltu)
    );

    /* Fake UART peripheral. Located at 0x20000000. */
    assign uart_write = mem_wen && (mem_addr == 32'h20000000);
    always @(posedge reset)
    begin
        uart_out <= 0;
    end

    always @(posedge clk)
    begin
        if (uart_write)
        begin
            uart_out <= mem_wdata;
        end

        uart_send <= uart_write;
    end

    /* Instruction decoder and control unit. */
    instr iu_module(
        .iword(idata),
        .alu_eq(alu_eq),
        .alu_lt(alu_lt),
        .alu_ltu(alu_ltu),
        .pc_src_select(pc_src_select),
        .mem_width(mem_width),
        .mem_wen(mem_wen),
        .imm(imm),
        .relative_addr(relative_addr),
        .reg_waddr(reg_waddr),
        .reg_raddr1(reg_raddr1),
        .reg_raddr2(reg_raddr2),
        .reg_wdata_sel(reg_wdata_sel),
        .reg_wen(reg_wen),
        .alu_op(alu_op),
        .alu_alt_op(alu_alt_op),
        .alu_src1_sel(alu_src1_sel),
        .alu_src2_sel(alu_src2_sel)
    );

    assign mem_addr = alu_out;

endmodule
