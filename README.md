# rascal-cpu

A simple RISC-V CPU implementation in verilog. Implements the RV32I standard.

This is a for-fun personal project. I'm not planning to turn this into a professional-grade CPU.

## Project status
Incomplete but runs some simple test programs.

## License
BSD license.

